  const test_url = 'https://jsonplaceholder.typicode.com/posts/'
  const g_basic_info_url = 'http://192.168.1.101/cgi-bin/basic_info'
  const g_board_blink_url = 'http://192.168.1.101/cgi-bin/board_blink'
  const g_weather_get_url = 'http://192.168.1.101/cgi-bin/weather_get'
  const g_board_control_url = 'http://192.168.1.101/cgi-bin/board_control'
  //   const g_lianyungang_weather_url = 'https://api.seniverse.com/v3/weather/now.json?key=SwwwfskBjB6fHVRon&location=lianyungang&language=zh-Hans&unit=c'
  const g_haizhou_weather_url = 'https://devapi.qweather.com/v7/weather/now?location=101191006&key=fe8eab50a7a14e699204b93c8ee819fc&gzip=n'

  const RED_LED = "dev1"
  const BLUE_LED = "dev0"
  let g_board_control_red = {
      id: RED_LED,
      code: 0,
  }
  let g_board_control_blue = {
      id: BLUE_LED,
      code: 0,
  }
  let g_blink_data = {
      enable: 0,
  }
  let g_weather_get_data = {
      weather: 0,
  }
  /* 遍歷獲取到的 json 對象 */
  //let data = null
  //   $('#btn_blink').click(function () {
  //       g_blink_data['enable'] ^= 0x01;
  //       $.ajax({
  //           url: g_board_blink_url,
  //           async: true,
  //           contentType: "application/json;charset=utf-8",
  //           data: JSON.stringify(g_blink_data),
  //           type: "POST"
  //       })
  //   })
  $('#btn_blink').click(function () {
      g_weather_get_data['weather'] = 1;
      $.ajax({
          url: g_weather_get_url,
          async: true,
          contentType: "application/json;charset=utf-8",
          data: JSON.stringify(g_weather_get_data),
          type: "POST"
      })
  })

  let g_basic_info_data = null;
  let g_basic_info_data_payload = null;
  let g_basic_info_data_json = null;
  let g_basic_info_temp_value = 0;
  let g_basic_info_baro_value = 0;

  let g_weather_data = null;
  let g_weather_data_json = null;
  let g_weather_data_now_json = null;

  const sleep = time => {
      return new Promise(resolve => setTimeout(resolve, time))
  }

  function show_basic_info() {
      $.get(g_basic_info_url, function (g_basic_info_data) {
          console.log(`BASIC 1`)
          console.log(g_basic_info_data);
          g_basic_info_data_json = $.parseJSON(g_basic_info_data);
          g_basic_info_data_payload = g_basic_info_data_json['payload'];
          g_basic_info_temp_value = g_basic_info_data_payload['temp'];
          g_basic_info_baro_value = g_basic_info_data_payload['baro'];
          //   console.log(g_basic_info_data_payload['id4'].slice(-14, -9));
          /* 僅僅現實時間 */
          //$('#time_show').text(g_basic_info_data_payload['id4'].slice(-14, -9));
          $('#time_show').text(g_basic_info_data_payload['id4']);
          $('#pressure').text("氣壓:" + g_basic_info_baro_value + " Pa");
          $('#temperature').text("溫度:" + g_basic_info_temp_value / 10 + " 攝氏度");
          console.log(`BASIC 2`)
      })
  }

  function weather_str2num(weather_str) {
      if (weather_str == "晴")
          return 0;
      else if (weather_str == "雨")
          return 1;
      else if (weather_str == "多云")
          return 2;
  }

  function show_weather_info() {
      $.get(g_haizhou_weather_url,
          function (g_weather_data) {
              console.log(`WEATHER 1`);
              console.log(typeof g_weather_data);
              //console.log(JSON.stringify(g_weather_data));
              g_weather_data_json = $.parseJSON(g_weather_data);
              g_weather_data_now_json = g_weather_data_json['now'];
              console.log(g_weather_data_now_json['text']);
              $('#humidity').text("海州天氣:" + g_weather_data_now_json['text']);
              g_weather_get_data['weather'] = weather_str2num(g_weather_data_now_json['text']);
              $.ajax({
                  url: g_weather_get_url,
                  async: true,
                  contentType: "application/json;charset=utf-8",
                  data: JSON.stringify(g_weather_get_data),
                  type: "POST"
              })
              console.log(`WEATHER 2`)
          }, 'text')
  }

  show_basic_info();
  show_weather_info();

  /* 定義一個定時器，周期地獲取 basic_info 信息 */
  self.setInterval(show_basic_info, 30000)
  self.setInterval(show_weather_info, 360000)

  $('#btn_red').click(function () {
      g_board_control_red['id'] = RED_LED;
      g_board_control_red['code'] ^= 0x01;
      console.log(`RED 1`)
      console.log(g_board_control_red['code']);
      $.ajax({
          url: g_board_control_url,
          async: true,
          contentType: "application/json;charset=utf-8",
          data: JSON.stringify(g_board_control_red),
          type: "POST"
      })
      console.log(`RED 2`)
  })

  $('#btn_blue').click(function () {
      g_board_control_blue['id'] = BLUE_LED;
      g_board_control_blue['code'] ^= 0x01;
      console.log(`BLUE 1`)
      $.ajax({
          url: g_board_control_url,
          async: true,
          contentType: "application/json;charset=utf-8",
          data: JSON.stringify(g_board_control_blue),
          type: "POST"
      })
      console.log(`BLUE 2`)
  })

  //while (false) {}
  //   const http = new XMLHttpRequest()
  //   http.open('GET', test_url)
  //   http.onreadystatechange = function () {
  //     if (this.readyState == 4 && this.status == 200) {
  //       console.log(http.responseText)
  //       console.log('OK')
  //     }
  //   }

  //   let fetch_response = null;
  //   let fetch_data = null;
  //   const fetch_param = {
  //       method: 'POST',
  //       mode: 'cors',
  //       headers: {
  //           'Content-Type': 'application/json;charset=utf-8',
  //       },
  //       body: JSON.stringify({
  //           enable: 1
  //       }),
  //   }
  //   fetch(test_url, fetch_param)
  //       .then(fetch_response => {
  //           return fetch_response.json();
  //       })
  //       .then(fetch_data => {
  //           console.log(fetch_data);
  //       })