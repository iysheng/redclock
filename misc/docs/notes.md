## 记录 factory 工程的代码笔记

### main 函数流程图
``` C

/* 默认的 webnet 会话处理函数集合 */
const struct webnet_session_ops _default_session_ops =
{
    /* webnet session 处理函数 */
    _webnet_session_handle,
    RT_NULL
};

int main(void)
    wifi_init()
        /* 注册一个 wlan 设备事件准备好的回调函数 */
        rt_wlan_register_event_handler(RT_WLAN_EVT_READY, wifi_ready_handler, NULL);
            /* adb 相关的服务线程倾听 5555 端口 */
            adb_socket_init();
                ret = adb_tcpip(5555);
                    /* 创建了一个 adbd-sk 线程，倾听 5555 端口 */
                    tid = rt_thread_create("adbd-sk", tcp_server, (void *)port, 1280, 22, 20);
    basic_init();
        /* LED 灯管脚配置 */
        rt_pin_mode(BLUE_LED, PIN_MODE_OUTPUT);
        rt_pin_mode(RED_LED, PIN_MODE_OUTPUT);
        rt_pin_write(BLUE_LED, PIN_HIGH);
        rt_pin_write(RED_LED, PIN_HIGH);
        /* 注册 webnet_session */
        webnet_cgi_register("basic_info", cgi_basic_info);
        webnet_cgi_register("board_control", cgi_board_control);
    sys_monitor_init();
        /* 创建一个 sys 线程 */
        rt_thread_t tid = rt_thread_create("sys", sys_monitor_thread, NULL, 2048, 15, 5);
        /* sys 线程的循环处理函数 */
            while (1)
            {
                /* 统计系统运行时间 */
                sys_monitor_handler();
                /* 周期性校时 */
                ntp_sync_handler();
                /* 延时 30ms */
                rt_thread_mdelay(SYS_MONITOR_INTERVAL_MS);
            }
    web_init();
        rt_thread_t tid = rt_thread_create("web_init", web_init_thread, NULL, 512, 15, 5);
            /* 循环等待 wlan 初始化完成 */
            while (!rt_wlan_is_ready())
            {
                rt_thread_mdelay(500);
            }
            /* 如果 wlan 已经连接网络初始化 webnet，
             * webnet 是一个 HTTP 的协议的 Web 服务器
             * */
            webnet_init();
                tid = rt_thread_create(WEBNET_THREAD_NAME, webnet_thread, RT_NULL, WEBNET_THREAD_STACKSIZE, WEBNET_PRIORITY, 5);
                    /* webnet_thread 线程循环执行函数 */
                    /* 创建一个 TCP 类型的 socket 句柄 */
                    listenfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
                        /* 绑定 80 端口到这个 socket */
                        if (bind(listenfd, (struct sockaddr *) &webnet_saddr, sizeof(webnet_saddr)) == -1)
                        {
                        }
                        /* 倾听 80 端口 */
                        if (listen(listenfd, WEBNET_CONN_MAX) == -1)
                        {
                        }
                        /* 初始化 webnet WEBNET_EVENT_INIT 事件，关联到
                         * 空 webnet_session，核心是根据不同的宏定义，完成
                         * 对应的初始化，目前选择的宏是 WEBNET_USING_CGI */
                        webnet_module_handle_event(RT_NULL, WEBNET_EVENT_INIT);
                            webnet_module_cgi(session, event);
                                if (event == WEBNET_EVENT_INIT)
                                {
                                    /* set default cgi path */
                                    if (_cgi_root[0] == '\0')
                                    {
                                        /* 初始化默认的 cgi_root 为 /cgi-bin/ 目录 */
                                        strcpy(_cgi_root, "/cgi-bin/");
                                    }
                                }
                        for (;;)
                        {
                            FD_ZERO(&readset);
                            FD_ZERO(&writeset);
                            /* 服务端的倾听句柄放在读端 */
                            FD_SET(listenfd, &readset);
                            if (FD_ISSET(listenfd, &tempfds))
                            {
                                struct webnet_session* accept_session;
                                /* We have a new connection request */
                                /* 创建一个新的 webnet_session */
                                accept_session = webnet_session_create(listenfd);
                                /* 如果申请内存不成功，那么接受这个连接请求，并直接断开，即
                                 * 不再建立这条链路通讯 */
                                if (accept_session == RT_NULL)
                                {
                                    /* 内存不足导致无法正常申请会话空间 */
                                }
                                else
                                {
                                    /* add read fdset */
                                    /* 将新创建的 socket 句柄也关联到倾听的读 fds */
                                    FD_SET(accept_session->socket, &readset);
                                }
                            }
                            /* 根据读写句柄集合的实际变化，处理对应的读写请求
                             * 会遍历匹配的 session，匹配到对应 session 的句柄有读写请求，然后
                             * 执行对应的处理
                             */
                            webnet_sessions_handle_fds(&tempfds, &writeset);
                        }
        static void web_init_thread(void *param)
    bluetooth_init();
        while (wifi == NULL)
        {
            /* 查找 wifi 设备 */
            wifi = rt_device_find(WIFI_DEVICE_NAME);
            rt_thread_mdelay(500);
        }
        /* 检查 fal 文件系统的 bt_image 分区是否存在有效的蓝牙固件 */
        if (bluetooth_firmware_check() < 0)
        {
            return -1;
        }
        /* 创建一个 fal 的字符设备，类似 flash 的字符设备，暂时不知道这个目的是什么？ */
        bt_firmware = fal_char_device_create(BT_FIRMWARE_PARTITION_NAME);
        if (bt_firmware == NULL)
        {
            LOG_E("bt firmware device create failed");
            return -1;
        }
        rt_thread_t tid = rt_thread_create("bt_thread", bluetooth_thread, NULL, 1024, 15, 5);
            bt_stack_port_main();
                tid1 = rt_thread_create("bt_stack", bt_stack_main, RT_NULL, 4096, 5, 5);
                    /* 蓝牙协议栈的处理线程 */
                    bt_stack_port_main();
```
